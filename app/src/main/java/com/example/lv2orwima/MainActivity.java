package com.example.lv2orwima;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NameClickListener {

    private RecyclerView recycler;
    private RecyclerAdapter adapter ;
    private EditText etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupRecycler();
        setupRecyclerData();

        this.etName= (EditText) findViewById(R.id.etName);
    }

    private void setupRecycler(){
        recycler = findViewById(R.id.recycleView);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerAdapter(this);
        recycler.setAdapter(adapter);
    }

    private void setupRecyclerData(){
        List<String> data =new ArrayList<>();
        data.add("Ana");
        data.add("Marko");
        data.add("Pero");
        adapter.addData(data);
    }

    public void addCell(View view) {
        adapter.addNewCell(etName.getText().toString(), adapter.getItemCount());
        etName.getText().clear();
    }


    @Override
    public void onDeleteClick(int position){
        adapter.removeCell(position);
    }

}