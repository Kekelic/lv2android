package com.example.lv2orwima;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private TextView tvName;
    private ImageView ivDelete;
    private NameClickListener clickListener;

    public NameViewHolder(@NonNull View itemView, NameClickListener listener) {
        super(itemView);
        this.clickListener =listener;
        tvName = itemView.findViewById(R.id.tvName);
        ivDelete = itemView.findViewById(R.id.ivDelete);
        ivDelete.setOnClickListener(this);
    }

    public void setName(String name){
        tvName.setText(name);
    }

    @Override
    public void onClick(View view) {
        clickListener.onDeleteClick(getAdapterPosition());
    }


}
